package orm

import (
	"gitlab.com/ptflp/orm-test/db"
	"gitlab.com/ptflp/orm-test/db/dao"
	"gitlab.com/ptflp/orm-test/infrastructure/db/migrate"
	"gitlab.com/ptflp/orm-test/infrastructure/db/scanner"
	"gitlab.com/ptflp/orm-test/utils"

	"go.uber.org/zap"
)

func NewOrm(dbConf utils.DB, scanner scanner.Scanner, logger *zap.Logger) (*dao.DAO, error) {
	var (
		orm *db.SqlDB
		err error
	)

	orm, err = db.NewSqlDB(dbConf, scanner, logger)
	if err != nil {
		logger.Fatal("error init db", zap.Error(err))
	}
	migrator := migrate.NewMigrator(orm.DB, dbConf, scanner)
	err = migrator.Migrate()
	if err != nil {
		logger.Fatal("migrator err", zap.Error(err))
	}
	return orm.DAO, nil
}
