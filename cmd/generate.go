package main

import (
	"log"

	"gitlab.com/ptflp/orm-test/genstorage"
)

// go build generate.go
// ./generate -fileName="../genstorage/models/test_model.go" - создаем директорию с файлами хранилища в корневом каталоге

func main() {
	storage, err := genstorage.NewStorage()
	if err != nil {
		log.Fatal(err)
	}

	err = storage.CreateStorageFiles()
	if err != nil {
		log.Fatal(err)
	}
}
